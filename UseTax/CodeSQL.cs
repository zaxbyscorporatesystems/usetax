﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace UseTax
{
    class CodeSQL : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public object CmdScalar(string con, string sqlCmd)
        {
            object rslt = 0;
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    sqlCon.Open();
                    using (SqlCommand sqlQ = new SqlCommand())
                    {
                        sqlQ.Connection = sqlCon;
                        sqlQ.CommandText = sqlCmd;
                        rslt = sqlQ.ExecuteScalar();
                    }
                    //sqlCon.Close();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return rslt;
        }

        public DataSet CmdDataset(string con, string sqlCmd)
        {
            DataSet dsRslt = new DataSet();
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    sqlCon.Open();
                    using (SqlDataAdapter sqlDap = new SqlDataAdapter())
                    {
                        sqlDap.SelectCommand = new SqlCommand(sqlCmd, sqlCon);
                        sqlDap.Fill(dsRslt);
                    }
                    //sqlCon.Close(); 
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return dsRslt;
        }

        public int CmdNonQuery(string con, string sqlCmd)
        {
            int rslt = 0;
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    sqlCon.Open();
                    using (SqlCommand sqlQ = new SqlCommand())
                    {
                        sqlQ.Connection = sqlCon;
                        sqlQ.CommandText = sqlCmd;
                        rslt = sqlQ.ExecuteNonQuery();
                    }
                    //sqlCon.Close(); 
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return rslt;
        }


    }

}
