﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Data;

namespace UseTax
{
    class CodeEmail : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        
        
        public string ExtMsg
        { set; get; }

        /// <summary>Send the email message.</summary>
        /// <returns>True if sent, false if error encountered.</returns>
        public bool SendEmail()
        {
            bool rslt = true;
            string recEmailFyle = string.Empty;
            try
            {

                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.Subject = "Monthly Use Tax File";
                StringBuilder sbMessage = new StringBuilder();
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("email.zaxbys.com");
                StringBuilder sbRecEmail = new StringBuilder();
                
                string emailTo = @Properties.Settings.Default.EmailTo.ToString();
                string emailFrom = @Properties.Settings.Default.EmailFrom.ToString();
                string emailFromAlias= @Properties.Settings.Default.EmailFromAlias.ToString();
                string emailcc= @Properties.Settings.Default.EmailCC.ToString();
                message.To.Add(emailTo);
                message.CC.Add(emailcc);
                message.From = new System.Net.Mail.MailAddress(emailFrom, emailFromAlias);
                //sbMessage.Append("-----Rerun of 2/8/2017 file with 699 corrections-----" + Environment.NewLine);
                //sbMessage.Append(Environment.NewLine);
                //sbMessage.Append(Environment.NewLine);
                sbMessage.Append($"The Use Tax job has completed it's monthly run.  The file is atttached.");
                sbMessage.Append(ExtMsg);
                string strAttachmentLoc = @Properties.Settings.Default.FileSavedAs.ToString();
                Attachment myAttach = new Attachment(strAttachmentLoc);
                message.Attachments.Add(myAttach);
                message.Priority = MailPriority.High;
                message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                message.Body = sbMessage.ToString();
                smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = false;
                smtp.Send(message);

                smtp.Dispose();
                message.Dispose();
            }
            catch (Exception ex)
            {
                rslt = false;
            }
            return rslt;
        }
    }
    }
