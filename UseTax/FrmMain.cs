﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
//using Microsoft.Office.Interop.Excel;
using Application = System.Windows.Forms.Application;

namespace UseTax
{
    public partial class FrmMain : Form
    {
        private const string KNbrFmt = "#,###,##0.00",
            KStrFmt = "@";

        string autorun = Properties.Settings.Default.AutoRun.ToLower();


        public FrmMain()
        {
            InitializeComponent();
        }

        
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            DateTime dteCurrent = DateTime.Now;
            dteCurrent = dteCurrent.AddMonths(-1);
            dtpkStart.Value = new DateTime(dteCurrent.Year, dteCurrent.Month, 1);
            dtpkEnd.Value = dtpkStart.Value.AddMonths(1).AddDays(-1);
            
            if (autorun=="yes")
            {
                getReport();
            }
        }

        private void cmdGetReport_Click(object sender, EventArgs e)
        {
            getReport();            
        }

        
        // methods---------------------

        private void getReport()
        {
            string fldrData = Application.StartupPath,
                fyleOfStates = "States.txt",
                fyleOfItems = "Items.txt",
                pfgCustNums,
                invoiceNums;
            StringBuilder sbStates = new StringBuilder(),
                sbSql = new StringBuilder(); ;
            fldrData += @"\";
            fyleOfStates = string.Format(@"{0}data\{1}", fldrData, fyleOfStates);
            fyleOfItems = string.Format(@"{0}data\{1}", fldrData, fyleOfItems);
            //lblStart.Text = $"{fyleOfStates}  {fyleOfItems}";
            if (DateTime.Compare(dtpkStart.Value, dtpkEnd.Value) >= 0)
            {
                MessageBox.Show(@"Start date must be before end date.", @"Correction Needed", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
            Cursor = Cursors.WaitCursor;
            // get pfg cust numbers for ZAX stores in states
            try
            {
                string[] states = File.ReadAllLines(fyleOfStates),
                    items = File.ReadAllLines(fyleOfItems);
                bool first = true;
                foreach (string state in states)
                {
                    sbStates.Append(first ? string.Format("'{0}'", state) : string.Format(",'{0}'", state));
                    first = false;
                }
                using (CodeSQL clsSql = new CodeSQL())
                {
                    //sbSql.Append("SELECT[Store_ID],[State],[Licensee_Code] FROM[Matrix_All_Stores_vw] where[Licensee_Code] = 'ZAX'");
                    //sbSql.Append(string.Format(" and [State] in ({0})", sbStates));
                    //sbSql.Append(" order by [State],[Store_ID]");
                    sbSql.Append("SELECT [store_id],[store_city],[store_state],[pfg_cust_num] FROM [apvcust] where [comp_code]='ZAX'");
                    sbSql.Append(string.Format(" and [store_state] in ({0})", sbStates));
                    sbSql.Append(" and[vendor_group] like '%PFG%'");
                    sbSql.Append(" group by[store_state],[store_id],[store_city],[pfg_cust_num]");
                    DataSet dsPfgCustNums = clsSql.CmdDataset(Properties.Settings.Default.conView, sbSql.ToString());
                    pfgCustNums = string.Empty;
                    first = true;
                    if (dsPfgCustNums.Tables.Count > 0 && dsPfgCustNums.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow drCustNum in dsPfgCustNums.Tables[0].Rows)
                        {
                            pfgCustNums += first ? string.Format("'{0}'", drCustNum["pfg_cust_num"]) : string.Format(",'{0}'", drCustNum["pfg_cust_num"]);
                            first = false;
                        }
                        // get invoice numbers
                        GC.Collect();
                        sbSql.Clear();
                        sbSql.Append("SELECT [rec_id],[customer_num],[invoice_num],[date_invoice] FROM [apinvhdr]");
                        sbSql.Append(string.Format(" where [customer_num] in ({0})", pfgCustNums));
                        sbSql.Append(" and[vend_supplier_id] in ('775','HALE','LESTE','MILTN','PFGCD','POWEL','TEMPL')");
                        sbSql.Append(string.Format(" and [date_invoice] between '{0}' and '{1}'", dtpkStart.Value.ToShortDateString(), dtpkEnd.Value.ToShortDateString()));
                        sbSql.Append("order by [date_invoice]");
                        DataSet dsInvoices = clsSql.CmdDataset(Properties.Settings.Default.conView, sbSql.ToString());
                        if (dsInvoices.Tables.Count > 0 && dsInvoices.Tables[0].Rows.Count > 0)
                        {
                            invoiceNums = string.Empty;
                            first = true;
                            foreach (DataRow dtInvoice in dsInvoices.Tables[0].Rows)
                            {
                                invoiceNums += first ? string.Format("'{0}'", dtInvoice["invoice_num"]) : string.Format(",'{0}'", dtInvoice["invoice_num"]);
                                first = false;
                            }
                            // get the invoice detail lines
                            //GC.Collect();
                            sbSql.Clear();
                            sbSql.Append("SELECT [rec_id],[invoice_num],[customer_num],[vend_supplier_id],[item_number],[item_desc],[account_code],[account_desc],[quantity],[uom],[price],[item_tax],[item_tax_percent]");
                            sbSql.Append(" FROM [apinvdet]");
                            sbSql.Append(" where ([account_code] like '40004%' or [account_code] like '51250%') and(");
                            first = true;
                            bool firstNotLike = true;
                            foreach (string item in items)
                            {
                                if (item.StartsWith("like"))
                                {
                                    if (first)
                                    {
                                        sbSql.Append(string.Format(" [item_desc] like '%{0}%'", stripIt(item)));
                                    }
                                    else
                                    {
                                        sbSql.Append(string.Format(" or [item_desc] like '%{0}%'", stripIt(item)));
                                    }
                                    first = false;
                                }
                                else
                                {
                                    if (firstNotLike)
                                    {
                                        sbSql.Append(string.Format(") and [item_desc] not like '%{0}%'", stripIt(item)));
                                    }
                                    else
                                    {
                                        sbSql.Append(string.Format(" and [item_desc] not like '%{0}%'", stripIt(item)));
                                    }
                                    firstNotLike = false;
                                }

                            }
                            sbSql.Append(string.Format(" and [invoice_num] in ({0})", invoiceNums));
                            DataSet dsInvDetailLynes = clsSql.CmdDataset(Properties.Settings.Default.conView, sbSql.ToString());
                            if (dsInvDetailLynes.Tables.Count > 0 && dsInvDetailLynes.Tables[0].Rows.Count > 0)
                            {
                                //add store_id, state and price extended
                                // loop with custNum from apvcust?????
                                // dgUseTaxItems.DataSource = dsInvDetailLynes.Tables[0];

                                string spreadName = @Properties.Settings.Default.FileSavedAs;

                                string  lastColHeader = "N1";

                                if (File.Exists(spreadName))
                                {
                                    File.Delete(spreadName);
                                }
                                object misValue = System.Reflection.Missing.Value;
                                Microsoft.Office.Interop.Excel.Application xlApp = new Microsoft.Office.Interop.Excel.Application();
                                Microsoft.Office.Interop.Excel.Workbook xlWorkBook = xlApp.Workbooks.Add();
                                Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.Add();     // total of 7 worksheets...

                                //GL40.1 column headers
                                // _f39:GLC-COMPANY*	_f44:GLC-FISCAL-YEAR	_f45:GLC-ACCT-PERIOD	_f46:GLC-SYSTEM*	_f48:GLC-JE-TYPE	F49 JE   _f67r0	D:1:5: f68:GLT-TO-COMPANY	D:1:5: f69:GLT-ACCT-UNIT	D:1:5: f70:GLT-ACCOUNT	D:1:5: f75:GLT-TRAN-AMOUNT	D:1:5: f81:GLT-DESCRIPTION
                                xlWorkSheet.Cells[1, 1] = "Store ID";
                                xlWorkSheet.Cells[1, 2] = "City";
                                xlWorkSheet.Cells[1, 3] = "State";
                                xlWorkSheet.Cells[1, 4] = "Vendor ID";
                                xlWorkSheet.Cells[1, 5] = "Cust Number";
                                xlWorkSheet.Cells[1, 6] = "Invoice Number";
                                xlWorkSheet.Cells[1, 7] = "Invoice Date";
                                xlWorkSheet.Cells[1, 8] = "Acct Code";
                                xlWorkSheet.Cells[1, 9] = "Item Number";
                                xlWorkSheet.Cells[1, 10] = "Description";
                                xlWorkSheet.Cells[1, 11] = "Qty";
                                xlWorkSheet.Cells[1, 12] = "Unit Price";
                                xlWorkSheet.Cells[1, 13] = "Total Price";
                                xlWorkSheet.Cells[1, 14] = "Item Tax";

                                var headerRange = xlWorkSheet.Range["A1", lastColHeader];
                                headerRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                                //Set the header-row bold
                                xlWorkSheet.Range["A1", lastColHeader].EntireRow.Font.Bold = true;
                                xlWorkSheet.Range["A1", lastColHeader].EntireRow.Font.Italic = true;
                                xlWorkSheet.Name = "Invoice Details for Use Tax";
                                int columnNumber = 0,
                                    iRow = 1;
                                decimal dQty,
                                    dPrice,
                                    dExtPrice;
                                try
                                {
                                    //GC.Collect();
                                    if (dsInvDetailLynes.Tables.Count > 0 && dsInvDetailLynes.Tables[0].Rows.Count > 0)
                                    {
                                        foreach (DataRow drDlyne in dsInvDetailLynes.Tables[0].Rows)
                                        {
                                            //iRow++;
                                            //columnNumber = 0;
                                            DataRow[] drTmp = dsPfgCustNums.Tables[0].Select(string.Format("[pfg_cust_num]='{0}'", drDlyne["customer_num"])),
                                                drDateInv = dsInvoices.Tables[0].Select(string.Format("[invoice_num]='{0}'", drDlyne["invoice_num"]));
                                            foreach (DataRow dRow in drTmp)
                                            {
                                                iRow++;
                                                columnNumber = 0;

                                                //try
                                                //{
                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KStrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = dRow["store_id"];
                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KStrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = dRow["store_city"];
                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KStrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = dRow["store_state"];
                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KStrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = drDlyne["vend_supplier_id"];
                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KStrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = drDlyne["customer_num"];
                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KStrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = drDlyne["invoice_num"];

                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KStrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = Convert.ToDateTime(drDateInv[0]["date_invoice"].ToString()).ToShortDateString();


                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KStrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = drDlyne["account_code"].ToString().Trim().Substring(0, 5);
                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KStrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = drDlyne["item_number"];
                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KStrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = drDlyne["item_desc"];
                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KStrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = drDlyne["quantity"];
                                                dQty = decimal.TryParse(drDlyne["quantity"].ToString(), out dQty) ? dQty : 1;
                                                dPrice = decimal.TryParse(drDlyne["price"].ToString(), out dPrice) ? dPrice : 0;
                                                dExtPrice = Math.Round(dQty * dPrice, 2);
                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KNbrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = dPrice;
                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KNbrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = dExtPrice;
                                                columnNumber++;
                                                xlWorkSheet.Cells[iRow, columnNumber].NumberFormat = KNbrFmt;
                                                xlWorkSheet.Cells[iRow, columnNumber] = drDlyne["item_tax"];
                                                GC.Collect();

                                                //    System.Threading.Thread.Sleep(100);
                                                //}
                                                //catch
                                                //{

                                                //}
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message + "\n row=" + iRow + " of " + dsInvDetailLynes.Tables[0].Rows.Count.ToString() + "\n column=" + columnNumber, @"Error in Loop", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    xlWorkSheet.Columns.AutoFit();
                                    // Fix first row
                                    xlWorkSheet.Activate();
                                    xlWorkSheet.Application.ActiveWindow.SplitRow = 1;
                                    xlWorkSheet.Application.ActiveWindow.FreezePanes = true;
                                    // Now apply autofilter
                                    Microsoft.Office.Interop.Excel.Range firstRowL = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Rows[1];
                                    xlWorkBook.SaveAs(spreadName);
                                    xlWorkBook.Close(true, misValue, misValue);
                                    xlApp.Quit();

                                    //releaseObject(xlWorkSheet);
                                    releaseObject(xlWorkBook);
                                    releaseObject(xlApp);

                                    //GC.Collect();
                                    GC.WaitForPendingFinalizers();
                                    GC.Collect();

                                    releaseObject(xlWorkSheet);
                                    Process.Start(spreadName);
                                    Cursor = Cursors.Default;
                                    return;

                                }
                                //[],[],[],[],[],[account_code],[account_desc],[],[uom],[price],[item_tax]

                                xlWorkSheet.Columns.AutoFit();
                                // Fix first row
                                xlWorkSheet.Activate();
                                xlWorkSheet.Application.ActiveWindow.SplitRow = 1;
                                xlWorkSheet.Application.ActiveWindow.FreezePanes = true;
                                // Now apply autofilter
                                Microsoft.Office.Interop.Excel.Range firstRow = (Microsoft.Office.Interop.Excel.Range)xlWorkSheet.Rows[1];
                                xlWorkBook.SaveAs(spreadName);
                                xlWorkBook.Close(true, misValue, misValue);
                                xlApp.Quit();

                                //releaseObject(xlWorkSheet);
                                releaseObject(xlWorkBook);
                                releaseObject(xlApp);

                                //GC.Collect();
                                GC.WaitForPendingFinalizers();
                                GC.Collect();

                                releaseObject(xlWorkSheet);
                                if (autorun == "yes")
                                {
                                    mailit();
                                }
                                else
                                {
                                    Process.Start(spreadName);
                                }

                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor = Cursors.Default;
                return;
            }
            Cursor = Cursors.Default;
            if(autorun=="yes")
            {
                Application.Exit();
            }
        }

        private void mailit()
        {
            CodeEmail clsEmail = new CodeEmail();
            bool returnval = clsEmail.SendEmail();

        }


        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            }
            catch (Exception ex)
            {
                //addToLogFile("Exception Occured while releasing object " + ex.Message);
            }
        }

        private string stripIt(string inStr)
        {
            string rslt = inStr.Substring(inStr.IndexOf('>')+1);

            return rslt.Trim();
        }
    }
}
